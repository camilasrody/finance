<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About the project
This is a academic console project in laravel for Corporate Systems class.<br> So you can use and manipulate as you wish, just clone the repository.<br>
This project simulate a sale process with some payment considerations.
<br>You can:
- do a user registration
- do a product registration
- purchase some of this products with a logged user
- do a list of users
- do a list of products
- do a exibition of the purchase statement
- delete product as long as you have a user logged.
- pay with debit or credit. Payments with credit have a 20% interest rate on the purchase price.

## Run the project
This command allows you to run the user registration without any restriction. <br> So you can register as many users as you like at once: <br>
<code> php artisan main </code> <br>

To use the desired user, just pass the id parameter. For example:<br>
<code>php artisan main <b>2</b> </code> <br>
this number "2" after "php artisan main" is referent to the user id.<br> So if you have 4 users in your database, you can pass numbers in a range of 1 to 4, for example.

## Assests
Maybe you have to install:
- php 7.0 >=
- composer
- nginx 
- to run server: php artisan server (on CLI) <br>
This is just a suggestion, you can see more of in <b>[this link here](https://laravel.com/docs/5.8/installation)</b> \o/




<p><b>PS</b>: All the database is in migrations so, use the migrations to build your database*</p>


## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
