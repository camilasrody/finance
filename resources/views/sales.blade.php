{!! Form::open(array('route' => 'route.name', 'method' => 'POST')) !!}
	<ul>
		<li>
			{!! Form::label('buyer_id', 'Buyer_id:') !!}
			{!! Form::text('buyer_id') !!}
		</li>
		<li>
			{!! Form::label('seller_id', 'Seller_id:') !!}
			{!! Form::text('seller_id') !!}
		</li>
		<li>
			{!! Form::label('total_price', 'Total_price:') !!}
			{!! Form::text('total_price') !!}
		</li>
		<li>
			{!! Form::submit() !!}
		</li>
	</ul>
{!! Form::close() !!}