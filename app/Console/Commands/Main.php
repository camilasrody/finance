<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Sale;
use App\Models\User;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class Main extends Command {

    public $client = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'main {user? : The ID of the current logged user}
                        {--queue= : Whether the job should be queued}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to start finance cli';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client([
                                       'headers' => ['Content-Type' => 'application/json']
                                   ]);
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        while (true) {
            $this->handle2();
            echo "\n\n===========================================\n\n\n\n";
        }
    }


    public function handle2()
    {
        $logged_user = User::where("id", $this->argument("user"))->first();

        $options = [
            'Cadastrar',
            'Listar usuarios',
            "Comprar",
            "Extrato de Compra",
            ( $this->isAdmin() ) ? "Adicionar produto" : "",
            ( $this->isAdmin() ) ? "Deletar produto" : "",
        ];
        $answer = 'Cadastrar';
        if ($logged_user) {
            $answer = $this->show_menu($logged_user, $options);
        }

        switch ($answer) {
            case "Cadastrar":
                $u = new User();
                $u->signup($this);
                break;
            case "Listar usuarios":
                $users = User::all([ 'id', 'name', 'email' ])->toArray();
                $this->table([ '#', 'Name', 'Email' ], $users);
                break;

            case "Comprar":
                $products = Product::all([ 'id', 'title', 'price' ])->toArray();
                $this->table([ '#', 'Título', 'Preço' ], $products);
                $p_id = $this->ask('Digite ID do produto que deseja comprar:');
                $p = Product::where('id', $p_id)->firstOrFail();

                $qnt = $this->ask("Quantas unidades de $p->title você gostaria?");
                $payment = $this->choice('payment_type', [ 'Débito', 'Crédito' ], 'Débito');
                $payment_type = ( $payment == 'Débito' );

                $s = new Sale();
                $s->product_id = $p->id;
                $s->buyer_id = $logged_user->id;
                $s->seller_id = $p->seller_id;
                $s->price = $p->price * $qnt;
                $s->payment_type = $payment_type;
                $s->product_id = $p->id;
                $s->status = 0;
                if ($s->save()) {
                    $data = $s->toArray();
                    $data['sale_id'] = $s->id;
                    // envia para o microserviço.
                    if ($this->client->post(env('API_PAYMENT').'/pay', [
                        'json' => $data,
                    ])){
                        $this->info("Compra registrada com sucesso! Pagamento pendente");
                    }else{
                        $this->error("deu ruim!");
                    }

                } else {
                    $this->error("Ocorreu um erro");
                }
                break;
            case "Extrato de Compra":
                $sales = Sale::where('buyer_id', $logged_user->id)->select([ 'id', 'price', 'created_at', 'payment_type', 'seller_id' ])->get();
                $sales = $sales->toArray();

                $user_total = 0;
                foreach ($sales as $k => $sale) {
                    $sales[$k]['payment_type'] = ( $sale['payment_type'] == 0 ) ? "Crédito" : "Débito";
                    $sales[$k]['total_price'] = $sale['price'];
                    if ($sale['payment_type'] == "Crédito") {
                        $sales[$k]['total_price'] = $sale['price'] * 1.20;
                    }
                    $user_total += $sales[$k]['total_price'];
                    $sales[$k]['seller'] = User::where('id', $sale['seller_id'])->firstOrFail()->name;
                    unset($sales[$k]['seller_id']);
                }
                $this->table([ '#', 'Preço', 'Data', 'Meio de pag.', 'Preço Total (+20%)', 'Vendedor'  ], $sales);
                echo "Você comprou um total de: R$ $user_total.";

                break;
            case "Deletar produto":
                $p = Product::where('id', $this->ask('Digite ID do produto:'))->delete();
                break;
            case "Adicionar produto":
                $p = new Product();
                $p->create($this, $logged_user);
                break;
        }
    }


    /**
     * @param User $logged_user
     * @param array $options
     * @return string
     */
    private function show_menu(User $logged_user, array $options): string
    {
        $this->info("\n");
        $this->info(" ============================ ");
        $this->info("   | Simulação de compra |");
        $this->info(" ============================ ");
        $this->info("\n");
        $this->info(" Bem vindo, " . $logged_user->name);
        return $this->choice('Selecione uma opção', $options);
    }


    private function isAdmin(): bool
    {
        // TODO: isAdmin
        return true;
    }
}
