<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model 
{

    protected $table = 'sales';
    public $timestamps = true;

    public function buyer()
    {
        return $this->belongsTo('App\Models\User', 'buyer_id');
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\User', 'seller_id');
    }

    public function product()
    {
        return $this->hasOne('Product');
    }

}