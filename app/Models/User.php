<?php

namespace App\Models;

use App\Console\Commands\Main;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function signup(Main $that)
    {
        $this->name = $that->ask('Digite seu nome completo:');
        $this->email = $that->ask('Digite seu email:');
        $this->password = $that->secret('Digite uma senha de 4-8 caracteres:');
        if($this->save()){
            $that->info("Cadastrado com sucesso!");
            return;
        }
        $that->error("Ocorreu um erro");
    }

    public function products()
    {
        return $this->hasMany('Product');
    }
}
