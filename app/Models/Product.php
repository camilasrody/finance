<?php

namespace App\Models;

use App\Console\Commands\Main;
use Illuminate\Database\Eloquent\Model;

class Product extends Model 
{

    protected $table = 'products';
    public $timestamps = true;

    public function create(Main $that, User $seller)
    {
        $this->title = $that->ask('Digite o título do novo produto:');
        $this->body = $that->ask('Digite uma descrição:');
        $this->manufacturer = $that->ask('Digite o fabricante:');
        $this->price = $that->ask('Digite o preço:');
        $this->seller_id = $seller->id;
        if($this->save()){
            $that->info("Produto salvo com sucesso!");
            return;
        }
        $that->error("Ocorreu um erro");
    }

    public function seller()
    {
        return $this->hasOne('User');
    }

    public function sale()
    {
        return $this->hasMany('Sale');
    }

}