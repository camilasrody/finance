<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
  public function webhook(Request $request) {
    
    $sale_id = $request->input('sale_id');

    if(\App\Models\Sale::where('status', 0)
          ->where('id', $sale_id)
          ->update(['status' => 1])){

      return response()->json([
            'ok' => true
        ]);
    }

    return response()->json([
        'error' => 'unknown'
    ]);
    
    
  }
}
